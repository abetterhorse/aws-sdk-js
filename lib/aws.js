var aws = require('./core');

// Use default API loader function
aws.apiLoader = require('./api_loader').load;

// Load the xml2js XML parser
aws.XML.Parser = require('./xml/node_parser');

// Load Node HTTP client
require('./http/node');

// Load all service classes
require('./services');

// Load custom credential providers
require('./credentials/ec2_metadata_credentials');
require('./credentials/environment_credentials');
require('./credentials/file_system_credentials');
require('./credentials/shared_ini_file_credentials');

// Setup default chain providers
aws.CredentialProviderChain.defaultProviders = [
  function () { return new aws.EnvironmentCredentials('AWS'); },
  function () { return new aws.EnvironmentCredentials('AMAZON'); },
  function () { return new aws.SharedIniFileCredentials(); },
  function () { return new aws.EC2MetadataCredentials(); }
];

// Update configuration keys
aws.util.update(aws.Config.prototype.keys, {
  credentials: function () {
    var credentials = null;
    new aws.CredentialProviderChain([
      function () { return new aws.EnvironmentCredentials('AWS'); },
      function () { return new aws.EnvironmentCredentials('AMAZON'); },
      function () { return new aws.SharedIniFileCredentials(); }
    ]).resolve(function(err, creds) {
      if (!err) credentials = creds;
    });
    return credentials;
  },
  credentialProvider: function() {
    return new aws.CredentialProviderChain();
  },
  region: function() {
    return process.env.AWS_REGION || process.env.AMAZON_REGION;
  }
});

// Reset configuration
aws.config = new aws.Config();

function AWS() {

  for ( var property in aws )
    this[property] = aws[property];
}

module.exports = AWS;
