# Constructor-flavored AWS SDK for JavaScript

A fork of version 2.1.13 of the official AWS SDK for JavaScript. This fork returns a class constructor, rather than a singleton.

Release notes for the original AWS SDK can be found at http://aws.amazon.com/releasenotes/SDK/JavaScript

## Installing

### In Node.js

```sh
npm install aws-sdk
```

In your node project:

```JavaScript
var AWS = require("aws-sdk");
var aws = new AWS();
```

### In the Browser

This fork of the AWS SDK currently isn't built to run in the browser.

## Usage and Getting Started

The original getting started guide can be found here:
http://docs.aws.amazon.com/AWSJavaScriptSDK/guide/

## License

This SDK is distributed under the
[Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0),
see LICENSE.txt and NOTICE.txt for more information.